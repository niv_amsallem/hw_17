#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <string.h>
#include "osrng.h"
#include "des.h"
#include "modes.h"


std::string encryptAES(std::string);
std::string decryptAES(std::string);

 CryptoPP::byte key[CryptoPP::AES::DEFAULT_KEYLENGTH], iv[CryptoPP::AES::BLOCKSIZE];

//this is a program that will test your library installation
//use it also to understand one of many ways to work with the library
//this program will ask you for an input, encrypt it using AES encryption and print the resault of the plaintext, ciphertext and finally the decrypted text
int main()
{
    std::string plainText, cipherText, decryptedText;
    std::getline(std::cin, plainText);
    std::cout << "original plain text is: " + plainText << std::endl;

	cipherText = encryptAES(plainText);
    std::cout << "cipher text is: " + cipherText << std::endl;

	decryptedText = decryptAES(cipherText);
    std::cout << "decrypted text is: " + decryptedText << std::endl;

	system("pause");
	return 0;
}

std::string encryptAES(std::string plainText){

    std::string cipherText;

	CryptoPP::AES::Encryption aesEncryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Encryption cbcEncryption(aesEncryption, iv);

	CryptoPP::StreamTransformationFilter stfEncryptor(cbcEncryption, new CryptoPP::StringSink(cipherText));
    //  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfEncryptor.Put(reinterpret_cast<const CryptoPP::byte*>(plainText.c_str()), plainText.length() + 1);
	stfEncryptor.MessageEnd();

	return cipherText;
}

std::string decryptAES(std::string cipherText){

	std::string decryptedText;

	CryptoPP::AES::Decryption aesDecryption(key, CryptoPP::AES::DEFAULT_KEYLENGTH);
	CryptoPP::CBC_Mode_ExternalCipher::Decryption cbcDecryption(aesDecryption, iv);

	CryptoPP::StreamTransformationFilter stfDecryptor(cbcDecryption, new CryptoPP::StringSink(decryptedText));
    //  StringSink created with new does not require explicit destruction, but this is only because THIS library is build this way
	stfDecryptor.Put(reinterpret_cast<const CryptoPP::byte*>(cipherText.c_str()), cipherText.size());
	stfDecryptor.MessageEnd();

	return decryptedText;
}